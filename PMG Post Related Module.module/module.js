$(document).ready(function() {
    var x = 0;
    var timer = setInterval( rssFix, 1000);
    function rssFix() {
        console.log(++x);
        if (++x === 20) {
            clearInterval(timer);
        }
        if ($('.hs-rss-item').length) {
            console.log("rss loaded");
            // function to run after form loads  
            $('.tabber-content .hs-rss-item-image-wrapper').each(function() {
               $(this).css("background-image","url("+$(this).find('img').attr('src')+")"); 
            });
            clearInterval(timer);
        }
    }
    $('.hs-related-blog-item-text').each( function(){
      $(this).find('.hs-related-blog-post-summary').parent('.hs-related-blog-byline').remove();
      $(this).find('.hs-related-blog-tags').parent('.hs-related-blog-byline').remove();     
      $(this).find('.hs-related-blog-author').remove();
      $(".hs-related-blog-byline").each(function () {
          $(this).html( $(this).html().replace(/by/g," ") );
      });
      $('.hs-related-blog-featured-image').each(function(){
        var imageUrl = $(this).attr('src');
        $(this).parent('.hs-related-blog-item-image-wrapper').css('background','url('+imageUrl+')');
      })
     
    })
});

//sticky sidebar   

   