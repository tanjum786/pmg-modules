let controller = new ScrollMagic.Controller();

// Run through all sections
let items = document.querySelectorAll(".content-section .box2");

items.forEach(function(element, index) {
    let height = element.clientHeight; //height of current element
   
    let image = document.getElementById("high" + (index + 1));
    let scene = new ScrollMagic.Scene({
        duration: height+200,
        triggerElement: element
    })
        .on("enter", function() {
            image.classList.add('active');
            console.log(index);
        }).on("leave", function(){
            image.classList.remove('active');
        })
        .addTo(controller);
});
