$("#byType").change(function(){
    $(this).find("option:selected").each(function(){
        var optionValue = $(this).attr("value");
        if(optionValue){
            $(".resSection").not("." + optionValue).fadeOut();
            $("." + optionValue).fadeIn();
        }
        if(optionValue == 'showall'){
          $('.resSection').fadeIn();
        }
    });
}).change();

$('.btnShowall').click( ()=>{ window.location.href = 'https://www.precisionmarketinggroup.com/pmg-resource-page-test-filter'})
