$('.select-box').mouseenter(function(){
    $(this).addClass('active');
})
$('.select-box').mouseleave(function(){
    $(this).removeClass('active');
})


$('.btnShowall').click( ()=>{ window.location.href = 'https://www-precisionmarketinggroup-com.sandbox.hs-sites.com/resource-page-testing'})

// filter code

$('#form_id').change(function(){
  let select1 = $('#byType option:selected').val();
  let select2 = $('select[name="topics"] option:selected').val();
  console.log(select1, select2);
})

// custom select
/*
Reference: http://jsfiddle.net/BB3JK/47/
*/

$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;
  
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());
  
    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);
  
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
        //if ($this.children('option').eq(i).is(':selected')){
        //  $('li[rel="' + $this.children('option').eq(i).val() + '"]').addClass('is-selected')
        //}
    }
  
    var $listItems = $list.children('li');
  
    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });
  
    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        if($this.val() != ''){
          $('.btnShowall').addClass('active');
          $('.button-box').addClass('active');
        }else{
          $('.btnShowall').removeClass('active');
          $('.button-box').removeClass('active');
        }
        console.log($this.val());
    });
  
    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});

// show hide All and Reset if option selected
// $('select').change(function(){
//   let select1 = $('#byType option:selected').val();
//   let select2 = $('select[name="topics"] option:selected').val();
//   console.log('option selected');
// })

