$(document).ready(function(){
  $('.t-member-imgmain , .mhlink').each(function(){
    var divshow = $(this).closest('.t-member-popbox').find('.t-member-popup-details').html();
    $(this).fancybox({
        type: 'inline',
        content: divshow,
        width : 480,
        maxWidth : 480
    });
  });
});